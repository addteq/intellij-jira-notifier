package com.addteq.intellij.plugin.jira.branch;

import com.addteq.intellij.plugin.jira.JiraBundle;
import com.addteq.intellij.plugin.jira.JiraRemoteApi;
import com.addteq.intellij.plugin.jira.utils.Globals;
import com.addteq.intellij.plugin.jira.utils.PropertyBean;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.startup.StartupActivity;
import git4idea.branch.GitBranchUtil;
import org.apache.commons.httpclient.methods.GetMethod;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static com.intellij.notification.NotificationListener.UrlOpeningListener;

/**
 * Created by vikashkumar on 6/28/16.
 */
public class CommentPuller implements StartupActivity{

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final Logger LOG = Logger.getInstance(getClass());
    public CommentPuller() {
        Globals.PROPERTY_BEAN = new PropertyBean();
        Globals.PROPERTY_BEAN.load();
    }

    public void pullRecentComment(final Project project) {

        final Runnable commentPuller = new Runnable() {
            public void run() {
                String branchName = "";
                try{
                    branchName = GitBranchUtil.getCurrentRepository(project).getCurrentBranchName();
                    LOG.info("branchName: "+branchName);
                }catch (Exception e) {
                    LOG.error(e);
                }

                PropertiesComponent propertiesComponent = PropertiesComponent.getInstance(project);
                int updated = propertiesComponent.getInt(branchName, 0);
                if(updated == 0) {
                    JsonObject response = getRecentComments(project, branchName, 0);
                    try{
                        JsonObject responseJsonObject = response;
                        if(responseJsonObject.has("errorMessages")) {
                            String errorMessage = responseJsonObject.getAsJsonObject("errorMessages").getAsString();
                            showErrorDialog(project, errorMessage);
                        }
                        LOG.info(responseJsonObject.toString());
                        int total =     responseJsonObject.getAsJsonPrimitive("total").getAsInt();
                        if(total>0) {
                            String commentUpdatedAt = "";
                            JsonArray commentArray = responseJsonObject.getAsJsonArray("comments");
                            StringBuilder commentHtmlNotification = new StringBuilder();
                            commentHtmlNotification.append("<div><h3>" + (total - updated) + " new comment(s) on "+branchName+".</h3><br><ol>");
                            for (int index = 0; index < commentArray.size(); index++) {
                                JsonObject commentObject = (JsonObject) commentArray.get(index);
                                String comment = commentObject.getAsJsonPrimitive("body").getAsString();
                                String commentId = commentObject.getAsJsonPrimitive("id").getAsString();
                                commentUpdatedAt = commentObject.getAsJsonPrimitive("updated").getAsString();
                                String url = getJiraIssueUrl(branchName, commentId);
                                LOG.info("Jira Server URL: "+url);
                                commentHtmlNotification.append("<li><a href='" + url + "'>" + comment + "</a></li>");

                            }
                            commentHtmlNotification.append("</ol></div>");
                            showMessage(project, commentHtmlNotification.toString());
                            propertiesComponent.setValue(branchName, total, 0);

                        }
                    }catch (Exception e) {
                        LOG.error(e);
                        showErrorDialog(project, "Please check your Jira Server URL and credential are correct." +
                                "Please check log file for more details.");
                    }
                } else {
                    int numberOfComments = propertiesComponent.getInt(branchName,0);
                    JsonObject response = getRecentComments(project, branchName, numberOfComments);
                    try{
                        JsonObject responseJsonObject = response;
                        LOG.info(responseJsonObject.toString());
                        if(responseJsonObject.has("errorMessages")) {
                            String errorMessage = responseJsonObject.getAsJsonObject("errorMessages").getAsString();
                            showErrorDialog(project, errorMessage);
                        }
                        int total =     responseJsonObject.getAsJsonPrimitive("total").getAsInt();
                        if(total>numberOfComments) {
                            String commentUpdatedAt = "";
                            JsonArray commentArray = responseJsonObject.getAsJsonArray("comments");
                            StringBuilder commentHtmlNotification = new StringBuilder();
                            commentHtmlNotification.append("<div><h3>" + (total - updated) + " new comment(s) on "+branchName+".</h3><br><ol>");
                            for (int index = 0; index < commentArray.size(); index++) {
                                JsonObject commentObject = (JsonObject) commentArray.get(index);
                                String comment = commentObject.getAsJsonPrimitive("body").getAsString();
                                String commentId = commentObject.getAsJsonPrimitive("id").getAsString();
                                commentUpdatedAt = commentObject.getAsJsonPrimitive("updated").getAsString();
                                String url = getJiraIssueUrl(branchName, commentId);
                                commentHtmlNotification.append("<li><a href='" + url + "'>" + comment + "</a></li>");

                            }
                            commentHtmlNotification.append("</ol></div>");
                            showMessage(project, commentHtmlNotification.toString());
                            propertiesComponent.setValue(branchName, total, 0);
                        }
                    }catch (Exception e) {
                        LOG.error("While reading json: "+e);
                        showErrorDialog(project, "Please check your Jira Server URL and credential are correct." +
                                "Please check log file for more details.");
                    }

                }
            }
        };
        final ScheduledFuture commentPullHandler = scheduler.scheduleAtFixedRate(commentPuller, 2, 6, TimeUnit.SECONDS);

        scheduler.schedule(new Runnable() {
            @Override
            public void run() {
                commentPullHandler.cancel(true);
            }
        }, 60*60, TimeUnit.SECONDS);
    }

    @Override
    public void runActivity(@NotNull Project project) {
        pullRecentComment(project);
    }
    private JsonObject getRecentComments(Project project, String jiraTicket, int startIndex) {
        JsonObject response = new JsonObject();
        try {
            GetMethod method = new GetMethod(getRestUrl(jiraTicket, startIndex));
            LOG.info("URI: "+method.getURI());
            JiraRemoteApi jiraRemoteApi = new JiraRemoteApi();
            response = jiraRemoteApi.executeMethod(method);

        }catch (Exception e) {
            LOG.error(e);
            showMessage(project, e.getMessage());
        }
        return response;
    }
    public String getRestUrl(String jiraTicketId, int startIndex) {
        String commentGetUrl = "";
        try {
            commentGetUrl = JiraBundle.message("jira.rest.url.comment.get", Globals.PROPERTY_BEAN.getServerUrl(), jiraTicketId, startIndex);

        }catch (Exception e) {
            LOG.error(e);
        }
        return commentGetUrl;
    }
    public String getJiraIssueUrl(String jiraTicketId, String commentId) {
        String commentFocus = JiraBundle.message("jira.url.comment.focus", Globals.PROPERTY_BEAN.getServerUrl(), jiraTicketId, commentId);
        return commentFocus;
    }

    public static void showMessage(final Project project, String html) {
        Notifications.Bus.notify(new Notification("Jira Comment Notifier", "Addteq Jira Notifier", html,
                NotificationType.INFORMATION, new UrlOpeningListener(true)), project);
    }
    private static void showErrorDialog(final Project project, String html) {
//        Notifications.Bus.notify(new Notification("Jira Alert Plugin", "Addteq Jira Notifier", html,
//                NotificationType.ERROR, new UrlOpeningListener(true)), project);
    }

    private Long convertStringDateToLong(String dateTime) {
        Long dateTimeLong=0L;
        try {
            dateTimeLong = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse(dateTime).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTimeLong;
    }
}