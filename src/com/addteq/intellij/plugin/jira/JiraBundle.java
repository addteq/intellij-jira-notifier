package com.addteq.intellij.plugin.jira;

import com.intellij.CommonBundle;
import com.intellij.reference.SoftReference;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.PropertyKey;

import java.lang.ref.Reference;
import java.util.ResourceBundle;

/**
 * Created by vikashkumar on 6/27/16.
 */
public class JiraBundle {

        private static Reference<ResourceBundle> ourBundle;
        @NonNls private static final String BUNDLE = "com.addteq.intellij.plugin.jira.JiraBundle";
        private JiraBundle() {
            // empty
        }

        public static String message(@NotNull @PropertyKey(resourceBundle = BUNDLE) String key, @NotNull Object... params) {
            return CommonBundle.message(getBundle(), key, params);
        }

        private static ResourceBundle getBundle() {
            ResourceBundle bundle = SoftReference.dereference(ourBundle);
            if (bundle == null) {
                bundle = ResourceBundle.getBundle(BUNDLE);
                ourBundle = new SoftReference<ResourceBundle>(bundle);
            }
            return bundle;
        }
}
