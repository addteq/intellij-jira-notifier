package com.addteq.intellij.plugin.jira.utils;

/**
 * Created by vikashkumar on 7/5/16.
 */

import com.addteq.intellij.plugin.jira.panel.JiraWindow;

import java.io.File;
import javax.swing.ImageIcon;

public class Globals {
    public static boolean DEBUG;
    public static PropertyBean PROPERTY_BEAN;
    public static JiraWindow TOP_WINDOW;

    public Globals() {

    }
}
