package com.addteq.intellij.plugin.jira.utils;

/**
 * Created by vikashkumar on 7/5/16.
 */

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.diagnostic.Logger;

public class PropertyBean {
    private String serverUrl;
    private String username;
    private String password;
    private boolean rememberPassword = true;
    private boolean soundWhenNotificationPopsUp = true;
    private boolean notificationInSystemBar = true;
    private final static Logger Logger =  com.intellij.openapi.diagnostic.Logger.getInstance(PropertyBean.class);

    public PropertyBean() {
    }

    public boolean isSoundWhenNotificationPopsUp() {
        return this.soundWhenNotificationPopsUp;
    }

    public void setSoundWhenNotificationPopsUp(boolean soundWhenNotificationPopsUp) {
        this.soundWhenNotificationPopsUp = soundWhenNotificationPopsUp;
    }

    public boolean isNotificationInSystemBar() {
        return this.notificationInSystemBar;
    }

    public void setNotificationInSystemBar(boolean notificationInSystemBar) {
        this.notificationInSystemBar = notificationInSystemBar;
    }

    public boolean isRememberPassword() {
        return this.rememberPassword;
    }

    public void setRememberPassword(boolean rememberPassword) {
        this.rememberPassword = rememberPassword;
    }

    public void save() {
        PropertiesComponent pc = PropertiesComponent.getInstance();
        if(!this.rememberPassword) {
            pc.setValue("serverUrl","");
            pc.setValue("username", "");
            pc.setValue("password", "");
        } else {
            pc.setValue("serverUrl", this.serverUrl);
            pc.setValue("username", this.username);
            pc.setValue("password", this.password);
        }

        pc.setValue("rememberPassword", Boolean.toString(this.rememberPassword));
        pc.setValue("soundWhenNotificationPopsUp", Boolean.toString(this.soundWhenNotificationPopsUp));
        pc.setValue("notificationInSystemBar", Boolean.toString(this.notificationInSystemBar));

    }

    public void load() {
        PropertiesComponent pc = PropertiesComponent.getInstance();
        this.serverUrl = pc.getValue("serverUrl");
        this.username = pc.getValue("username");
        this.password = pc.getValue("password");
        this.rememberPassword = Boolean.parseBoolean(pc.getValue("rememberPassword"));
        this.soundWhenNotificationPopsUp = Boolean.parseBoolean(pc.getValue("soundWhenNotificationPopsUp"));
        this.notificationInSystemBar = Boolean.parseBoolean(pc.getValue("notificationInSystemBar"));
        if(this.username == null) {
            this.username = "";
        }

        if(this.password == null) {
            this.password = "";
        }

        Logger.info("Jira Server configuration information loaded.");
    }

    public String getServerUrl() {
        return this.serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}