package com.addteq.intellij.plugin.jira;

/**
 * Created by vikashkumar on 6/27/16.
 */


import com.addteq.intellij.plugin.jira.utils.Globals;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.util.io.StreamUtil;
import com.intellij.openapi.vfs.CharsetToolkit;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.auth.AuthScope;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;

public class JiraRemoteApi {
    private final static Logger LOG = Logger.getInstance(JiraRemoteApi.class);
    JsonParser jsonParser = new JsonParser();
    @NotNull
    public JsonObject executeMethod(@NotNull HttpMethod method) {
        JsonObject response = new JsonObject();
        try {
            HttpClient client = createClient();
            int statusCode=0;
            try {
                statusCode = client.executeMethod(method);
            }catch (Exception e) {
                LOG.error(e);
                LOG.error("Status code: " + statusCode);
            }
            // may be null if 204 No Content received
            final InputStream stream = method.getResponseBodyAsStream();
            String entityContent = stream == null ? "" : StreamUtil.readText(stream, CharsetToolkit.UTF8);
            if (statusCode >= 200 && statusCode < 300) {
                response = jsonParser.parse(entityContent).getAsJsonObject();
            } else {
                if (method.getResponseHeader("Content-Type") != null) {
                    Header header = method.getResponseHeader("Content-Type");
                    if (header.getValue().startsWith("application/json")) {
                        if (entityContent.contains("errorMessages")) {
                            response = jsonParser.parse(entityContent).getAsJsonObject();
                            response.getAsJsonObject("errorMessages").getAsString();
                            LOG.error("Error occured while connecting to Jira: "+response.getAsJsonObject("errorMessage").getAsString());
                        }
                    }
                }
            }
            if (method.getResponseHeader("X-Authentication-Denied-Reason") != null) {
                Header header = method.getResponseHeader("X-Authentication-Denied-Reason");
                // only in JIRA >= 5.x.x
                if (header.getValue().startsWith("CAPTCHA_CHALLENGE")) {
                    LOG.error(JiraBundle.message("jira.failure.captcha"));
                    response.addProperty("errorMessages", "Jira captcha failure, looks like you already sufficient failed login attempt. " +
                            "You may like to login into your jira application manually and try again.");
                }
            }
            if (statusCode == HttpStatus.SC_UNAUTHORIZED) {
                LOG.error(JiraBundle.message("failure.login"));
                response.addProperty("errorMessages", "Unauthorized login error.");
            }
            String statusText = HttpStatus.getStatusText(method.getStatusCode());
            LOG.warn((statusText));
        } catch (Exception e) {
            response.addProperty("errorMessages", "Some unexpected error orrured. Please check log file for more details. To see log, go to menu Help->Show Log");
            LOG.error(e);
        }
        return response;
    }

    private HttpClient createClient() {
        HttpClient client = new HttpClient(new MultiThreadedHttpConnectionManager());
        configureHttpClient(client);
        return client;
    }

    protected void configureHttpClient(HttpClient client) {
        client.getParams().setConnectionManagerTimeout(3000);
        client.getParams().setSoTimeout(3*1000);
        if (true) {
            client.getParams().setCredentialCharset("UTF-8");
            client.getParams().setAuthenticationPreemptive(true);
            client.getState().setCredentials(AuthScope.ANY,
                    new UsernamePasswordCredentials(Globals.PROPERTY_BEAN.getUsername(),
                            Globals.PROPERTY_BEAN.getPassword()));
        }
    }
}

