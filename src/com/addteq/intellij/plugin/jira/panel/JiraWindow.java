package com.addteq.intellij.plugin.jira.panel;

import com.addteq.intellij.plugin.jira.utils.Globals;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import com.intellij.ui.content.ContentFactory.SERVICE;
import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * Created by vikashkumar on 7/11/16.
 */
public class JiraWindow implements ToolWindowFactory{

    ToolWindow windowRef;
    ContentFactory contentFactory;
    LoginPanel loginPanel;
    JBScrollPane panel = new JBScrollPane();
    JTextPane jtp = new JTextPane();
    private final static Logger LOG = Logger.getInstance(JiraWindow.class);

    public JiraWindow() {
        Globals.TOP_WINDOW = this;
        Globals.DEBUG = false;
    }

    public void createToolWindowContent(Project project, ToolWindow toolWindow) {
        this.loginPanel = new LoginPanel();
        this.contentFactory = SERVICE.getInstance();
        Content content = this.contentFactory.createContent(this.loginPanel, "", false);
        this.windowRef = toolWindow;
        this.windowRef.getContentManager().addContent(content);
    }
}