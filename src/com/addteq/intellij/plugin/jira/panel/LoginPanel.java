package com.addteq.intellij.plugin.jira.panel;

/**
 * Created by vikashkumar on 7/5/16.
 */


import com.addteq.intellij.plugin.jira.utils.Globals;
import com.addteq.intellij.plugin.jira.utils.PropertyBean;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.uiDesigner.core.Spacer;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class LoginPanel extends JPanel {
    private JTextField serverUrl;
    private JTextField username;
    private JPasswordField password;
    private JButton loginButton;
    private JCheckBox rememberPasswordCheckBox;
    private final static Logger LOG = Logger.getInstance(LoginPanel.class);
    public LoginPanel() {
        Globals.PROPERTY_BEAN = new PropertyBean();
        Globals.PROPERTY_BEAN.load();
        this.initComponents();
    }

    private void initComponents() {
        this.setLayout(new FormLayout("fill:d:noGrow,left:4dlu:noGrow,fill:d:grow,left:4dlu:noGrow,fill:d:grow",
                "center:d:noGrow,top:3dlu:noGrow,center:max(d;4px):noGrow,top:3dlu:noGrow,center:max(d;4px):noGrow,top:3dlu:noGrow,center:max(d;4px):noGrow,top:3dlu:noGrow,center:max(d;4px):noGrow"));
        JLabel jiraUrl = new JLabel();
        jiraUrl.setText("Server URL: ");
        CellConstraints cc = new CellConstraints();
        this.add(jiraUrl, cc.xy(1, 1));
        this.serverUrl = new JTextField();
        if(Globals.PROPERTY_BEAN.getServerUrl() != null && Globals.PROPERTY_BEAN.getServerUrl().length()>0) {
            this.serverUrl.setText(Globals.PROPERTY_BEAN.getServerUrl());
        }
        this.add(this.serverUrl, cc.xy(3, 1, CellConstraints.FILL, CellConstraints.DEFAULT));
        Spacer spacer1 = new Spacer();
        this.add(spacer1, cc.xy(5, 1, CellConstraints.FILL, CellConstraints.DEFAULT));

        JLabel label1 = new JLabel();
        label1.setText("Username :");
        this.add(label1, cc.xy(1, 3));

        this.username = new JTextField();
        if(Globals.PROPERTY_BEAN.getUsername() != null && Globals.PROPERTY_BEAN.getUsername().length() > 0) {
            this.username.setText(Globals.PROPERTY_BEAN.getUsername());
        }

        this.add(this.username, cc.xy(3, 3, CellConstraints.FILL, CellConstraints.DEFAULT));
        spacer1 = new Spacer();
        this.add(spacer1, cc.xy(5, 3, CellConstraints.FILL, CellConstraints.DEFAULT));

        JLabel label2 = new JLabel();
        label2.setText("Password :");
        this.add(label2, cc.xy(1, 5));


        this.password = new JPasswordField();
        if(Globals.PROPERTY_BEAN.getPassword() != null && Globals.PROPERTY_BEAN.getPassword().length() > 0) {
            this.password.setText(Globals.PROPERTY_BEAN.getPassword());
        }

        this.add(this.password, cc.xy(3, 5, CellConstraints.FILL, CellConstraints.DEFAULT));
        this.loginButton = new JButton();
        this.loginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                LoginPanel.this.actionPerformedButton(actionEvent);
            }
        });
        this.loginButton.setText("Save");
        this.add(this.loginButton, cc.xy(3, 9));
        this.rememberPasswordCheckBox = new JCheckBox();
        this.rememberPasswordCheckBox.setSelected(true);
        this.rememberPasswordCheckBox.setText("Remember Password");
        this.add(this.rememberPasswordCheckBox, cc.xy(3, 7));
    }

    public void actionPerformedButton(ActionEvent actionEvent) {
        if(this.password.getPassword() != null && this.password.getPassword().length > 0
                && this.username != null && this.username.getText().length() > 0
                && this.serverUrl!=null && this.serverUrl.getText().length()>0) {
            if(null == Globals.PROPERTY_BEAN || !this.username.getText().equals(Globals.PROPERTY_BEAN.getUsername())) {
                Globals.PROPERTY_BEAN = new PropertyBean();
            }

            Globals.PROPERTY_BEAN.setServerUrl(this.serverUrl.getText());
            Globals.PROPERTY_BEAN.setPassword(new String(this.password.getPassword()));
            Globals.PROPERTY_BEAN.setUsername(this.username.getText());
            Globals.PROPERTY_BEAN.setRememberPassword(this.rememberPasswordCheckBox.isSelected());
            Globals.PROPERTY_BEAN.save();
            LOG.info("Credential saved");
        }

    }
}
